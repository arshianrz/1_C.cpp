#include "shape.h"
#include <iostream>

using namespace std;

shape::shape()
{

}

void shape::setN(int N)
{
    n_=N;
}

int shape::getN()
{
    return n_;
}

void shape::display()
{
    for(int i=n_ ; i>=1 ; i--)
    {
        for(int j=1 ; j<=i ; j++)
        {
            cout<<"*";
        }
        cout<<endl;
    }
}

void shape::run()
{
    int a;

    cout<<"Enter a Number. \n";
    cin>>a;

    setN(a);
    getN();
    display();
}
